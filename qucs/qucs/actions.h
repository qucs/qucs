/***************************************************************************
                                  actions.h
                                 --------
    begin                : 2018
    copyright            : Qucs Team
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*!
 * \file actions.h
 * \Declaration of stuff related to actions.
 */

#ifndef QUCS_ACTIONS_H
#define QUCS_ACTIONS_H

enum arrow_dir_t{
  arr_up,
  arr_down,
  arr_left,
  arr_right
};

#endif
