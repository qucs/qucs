/***************************************************************************
                              schematic_file.cpp
                             --------------------
    begin                : Sat Mar 27 2004
    copyright            : (C) 2003 by Michael Margraf
    email                : michael.margraf@alumni.tu-berlin.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <deque>
#include <QtCore>
#include <QMessageBox>
#include <QDir>
#include <QDebug>
#include <QStringList>
#include <QPlainTextEdit>
#include "qt_compat.h"
#include <QTextStream>
#include <QList>
#include <QProcess>
#include <QDebug>

#include "qucs.h"
#include "node.h"
#include "schematic.h"
#include "diagrams/diagrams.h"
#include "paintings/paintings.h"
#include "components/spicefile.h"
#include "components/vhdlfile.h"
#include "components/verilogfile.h"
#include "components/libcomp.h"
#include "components/subcircuit.h" // BUG. move to qucsator.cc
#include "module.h"
#include "misc.h"
#include "sim/sim.h"
#include "io_trace.h"
#include "command.h"
#include "globals.h"
#include "docfmt.h"
#include "schematic_lang.h"


// Here the subcircuits, SPICE components etc are collected. It must be
// global to also work within the subcircuits.
SubMap FileList;


// -------------------------------------------------------------
// Creates a Qucs file format (without document properties) in the returning
// string. This is used to copy the selected elements into the clipboard.
QString SchematicModel::createClipboardFile()
{
  //something like
  // stringstream s
  // for (i : selectedItems){
  //   lang.print(s, i)
  // }
  // return s.str();

  return "";

#if 0
  int z=0;  // counts selected elements
  Diagram *pd;
  Painting *pp;

  QString s("<Qucs Schematic " PACKAGE_VERSION ">\n");

  // Build element document.
  s += "<Components>\n";
  for(auto pc : components()){
    if(pc->isSelected()) {
      QTextStream str(&s);
      incomplete();
      // saveComponent(str, pc);
      s += "\n";
      ++z;
    }
  }
  s += "</Components>\n";

  s += "<Wires>\n";
  for(auto pw : wires()){
    if(pw->isSelected()) {
      z++;
      if(pw->Label) if(!pw->Label->isSelected()) {
	s += pw->save().section('"', 0, 0)+"\"\" 0 0 0>\n";
	continue;
      }
      s += pw->save()+"\n";
    }
  }
  for(auto pn : nodes()) {
    if(pn->Label) if(pn->Label->isSelected()) {
      s += pn->Label->save()+"\n";  z++; 
    }
  }
  s += "</Wires>\n";

  s += "<Diagrams>\n";
  for(auto pd : diagrams()) {
    if(pd->isSelected()) {
      s += pd->save()+"\n";  z++;
    }
  }
  s += "</Diagrams>\n";

  s += "<Paintings>\n";
  for(auto pp : paintings()){
    if(pp->isSelected()){
      if(pp->Name.at(0) != '.') {  // subcircuit specific -> do not copy
        s += "<"+pp->save()+">\n";  z++;
      }
    }else{
    }
  }
  s += "</Paintings>\n";

  if(z == 0) return "";   // return empty if no selection

  return s;
#endif
}

// -------------------------------------------------------------
// Only read fields without loading them.
bool Schematic::loadIntoNothing(DocumentStream *stream)
{
  QString Line, cstr;
  while(!stream->atEnd()) {
    Line = stream->readLine();
    if(Line.at(0) == '<') if(Line.at(1) == '/') return true;
  }

  QMessageBox::critical(0, QObject::tr("Error"),
	QObject::tr("Format Error:\n'Painting' field is not closed!"));
  return false;
}

// -------------------------------------------------------------
// Paste from clipboard. into pe. wtf is pe?
bool Schematic::pasteFromClipboard(DocumentStream *stream, EGPList* pe)
{ untested();
#if 1
  QString Line;

  Line = stream->readLine();
  if(Line.left(16) != "<Qucs Schematic ")   // wrong file type ?
    return false;

  Line = Line.mid(16, Line.length()-17);
  VersionTriplet DocVersion = VersionTriplet(Line);
  if (DocVersion > QucsVersion) { // wrong version number ?
    if (!QucsSettings.IgnoreFutureVersion) {
      QMessageBox::critical(0, QObject::tr("Error"),
                            QObject::tr("Wrong document version: %1").arg(DocVersion.toString()));
      return false;
    }
  }

  // read content in symbol edit mode *************************
  if(isSymbolMode()) {
    incomplete(); // yikes.
    while(!stream->atEnd()) {
      Line = stream->readLine();
      if(Line == "<Components>") {
        if(!loadIntoNothing(stream)) return false; }
      else
      if(Line == "<Wires>") {
        if(!loadIntoNothing(stream)) return false; }
      else
      if(Line == "<Diagrams>") {
        if(!loadIntoNothing(stream)) return false; }
      else
      if(Line == "<Paintings>") {
	PaintingList pl;
	incomplete(); // ignore paintings.
        if(!DocModel.loadPaintings(stream, &pl)) return false;
      } else {
        QMessageBox::critical(0, QObject::tr("Error"),
		   QObject::tr("Clipboard Format Error:\nUnknown field!"));
        return false;
      }
    }

    return true;
  }

  // read content in schematic edit mode *************************
  SchematicModel x(NULL);
  try{
    x.parse(*stream);
  }catch( std::string ){
    incomplete();
//    messagebox?
  }

  // better interface for this kind of stuff?
  assert(!pe->count());
#ifndef USE_SCROLLVIEW
  x.toScene(*scene(), pe);
#endif
  DocModel.merge(x);

#endif
  return true;
}

// -------------------------------------------------------------
int Schematic::saveSymbolCpp (void)
{
  QFileInfo info (docName());
  QString cppfile = info.path () + QDir::separator() + DataSet;
  QFile file (cppfile);

  if (!file.open (QIODevice::WriteOnly)) {
    QMessageBox::critical (0, QObject::tr("Error"),
		   QObject::tr("Cannot save C++ file \"%1\"!").arg(cppfile));
    return -1;
  }

  DocumentStream stream (&file);

  // automatically compute boundings of drawing
  int xmin = INT_MAX;
  int ymin = INT_MAX;
  int xmax = INT_MIN;
  int ymax = INT_MIN;
  int x1, y1, x2, y2;
  int maxNum = 0;
  Painting * pp;

  stream << "  // symbol drawing code\n";
  for (pp = symbolPaintings().first (); pp != 0; pp = symbolPaintings().next ()) {
    if (pp->Name == ".ID ") continue;
    if (pp->Name == ".PortSym ") {
      if (((PortSymbol*)pp)->numberStr.toInt() > maxNum)
	maxNum = ((PortSymbol*)pp)->numberStr.toInt();
      x1 = ((PortSymbol*)pp)->cx_();
      y1 = ((PortSymbol*)pp)->cy_();
      if (x1 < xmin) xmin = x1;
      if (x1 > xmax) xmax = x1;
      if (y1 < ymin) ymin = y1;
      if (y1 > ymax) ymax = y1;
      continue;
    }
    pp->Bounding (x1, y1, x2, y2);
    if (x1 < xmin) xmin = x1;
    if (x2 > xmax) xmax = x2;
    if (y1 < ymin) ymin = y1;
    if (y2 > ymax) ymax = y2;
    stream << "  " << pp->saveCpp () << "\n";
  }

  stream << "\n  // terminal definitions\n";
  for (int i = 1; i <= maxNum; i++) {
    for (pp = symbolPaintings().first (); pp != 0; pp = symbolPaintings().next ()) {
      if (pp->Name == ".PortSym ")
	if (((PortSymbol*)pp)->numberStr.toInt() == i)
	  stream << "  " << pp->saveCpp () << "\n";
    }
  }

  stream << "\n  // symbol boundings\n"
	 << "  x1 = " << xmin << "; " << "  y1 = " << ymin << ";\n"
	 << "  x2 = " << xmax << "; " << "  y2 = " << ymax << ";\n";

  stream << "\n  // property text position\n";
  for (pp = symbolPaintings().first (); pp != 0; pp = symbolPaintings().next ())
    if (pp->Name == ".ID ")
      stream << "  " << pp->saveCpp () << "\n";

  file.close ();
  return 0;
}

// save symbol paintings in JSON format
int Schematic::saveSymbolJSON()
{
  QFileInfo info (docName());
  QString jsonfile = info.path () + QDir::separator()
                   + info.completeBaseName() + "_sym.json";

  qDebug() << "saveSymbolJson for " << jsonfile;

  QFile file (jsonfile);

  if (!file.open (QIODevice::WriteOnly)) {
    QMessageBox::critical (0, QObject::tr("Error"),
		   QObject::tr("Cannot save JSON symbol file \"%1\"!").arg(jsonfile));
    return -1;
  }

  QTextStream stream (&file);

  // automatically compute boundings of drawing
  int xmin = INT_MAX;
  int ymin = INT_MAX;
  int xmax = INT_MIN;
  int ymax = INT_MIN;
  int x1, y1, x2, y2;
  int maxNum = 0;
  Painting * pp;

  stream << "{\n";

  stream << "\"paintings\" : [\n";

  // symbol drawing code"
  for (pp = symbolPaintings().first (); pp != 0; pp = symbolPaintings().next ()) {
    if (pp->Name == ".ID ") continue;
    if (pp->Name == ".PortSym ") {
      if (((PortSymbol*)pp)->numberStr.toInt() > maxNum)
	maxNum = ((PortSymbol*)pp)->numberStr.toInt();
      x1 = ((PortSymbol*)pp)->cx_();
      y1 = ((PortSymbol*)pp)->cy_();
      if (x1 < xmin) xmin = x1;
      if (x1 > xmax) xmax = x1;
      if (y1 < ymin) ymin = y1;
      if (y1 > ymax) ymax = y1;
      continue;
    }
    pp->Bounding (x1, y1, x2, y2);
    if (x1 < xmin) xmin = x1;
    if (x2 > xmax) xmax = x2;
    if (y1 < ymin) ymin = y1;
    if (y2 > ymax) ymax = y2;
    stream << "  " << pp->saveJSON() << "\n";
  }

  // terminal definitions
  //stream << "terminal \n";
  for (int i = 1; i <= maxNum; i++) {
    for (pp = symbolPaintings().first (); pp != 0; pp = symbolPaintings().next ()) {
      if (pp->Name == ".PortSym ")
	if (((PortSymbol*)pp)->numberStr.toInt() == i)
	  stream << "  " << pp->saveJSON () << "\n";
    }
  }

  stream << "],\n"; //end of paintings JSON array

  // symbol boundings
  stream
    << "  \"x1\" : " << xmin << ",\n" << "  \"y1\" : " << ymin << ",\n"
    << "  \"x2\" : " << xmax << ",\n" << "  \"y2\" : " << ymax << ",\n";

  // property text position
  for (pp = symbolPaintings().first (); pp != 0; pp = symbolPaintings().next ())
    if (pp->Name == ".ID ")
      stream << "  " << pp->saveJSON () << "\n";

  stream << "}\n";

  file.close ();
  return 0;


}

// BUG: move to SchematicModel
namespace{
class sda : public ModelAccess{
public:
  sda(SchematicModel const& m, Schematic const& s) : _m(m), _s(s) {
  }
private: // ModelAccess
  SchematicModel const& schematicModel() const{
    incomplete();
    return _m;
  }
  std::string const& getParameter(std::string const&x) const{
    incomplete();
    return x;
  }
private:
  SchematicModel const& _m;
  Schematic const& _s;
};
}

// -------------------------------------------------------------
// Returns the number of subcircuit ports.
void Schematic::saveDocument() const
{
  // QFile file(DocName);
  // if(!file.open(QIODevice::ReadOnly)) {
  //   return -1;
  // }
  QFile file(DocName);
  file.open(QIODevice::WriteOnly | QIODevice::Truncate);
  DocumentStream stream(&file);

  sda a(DocModel, *this);

  // TODO: provide selection GUI
  auto D=docfmt_dispatcher["leg_sch"];
  D->save(stream, a);
}

// -------------------------------------------------------------
#if 0// moved
bool Schematic::loadProperties(QTextStream *stream)
// // TODO: move to frame::setParameters
void Schematic::setFrameText(int idx, QString s)
{
  if(s != FrameText[idx]){
    setChanged(true);
    FrameText[idx] = s;
    misc::convert2Unicode(FrameText[idx]);
  }else{
  }
}

// -------------------------------------------------------------
// // TODO: move to frame::setParameters
bool SchematicModel::loadProperties(QTextStream *stream)
{
  Schematic* d = _doc;
  bool ok = true;
  QString Line, cstr, nstr;
  while(!stream->atEnd()) {
    Line = stream->readLine();
    if(Line.at(0) == '<') if(Line.at(1) == '/') return true;  // field end ?
    Line = Line.trimmed();
    if(Line.isEmpty()) continue;

    if(Line.at(0) != '<') {
      QMessageBox::critical(0, QObject::tr("Error"),
		QObject::tr("Format Error:\nWrong property field limiter!"));
      return false;
    }
    if(Line.at(Line.length()-1) != '>') {
      QMessageBox::critical(0, QObject::tr("Error"),
		QObject::tr("Format Error:\nWrong property field limiter!"));
      return false;
    }
    Line = Line.mid(1, Line.length()-2);   // cut off start and end character

    cstr = Line.section('=',0,0);    // property type
    nstr = Line.section('=',1,1);    // property value
         if(cstr == "View") {
		d->ViewX1 = nstr.section(',',0,0).toInt(&ok); if(ok) {
		d->ViewY1 = nstr.section(',',1,1).toInt(&ok); if(ok) {
		d->ViewX2 = nstr.section(',',2,2).toInt(&ok); if(ok) {
		d->ViewY2 = nstr.section(',',3,3).toInt(&ok); if(ok) {
		d->Scale  = nstr.section(',',4,4).toDouble(&ok); if(ok) {
		d->tmpViewX1 = nstr.section(',',5,5).toInt(&ok); if(ok)
		d->tmpViewY1 = nstr.section(',',6,6).toInt(&ok); }}}}} }
    else if(cstr == "Grid") {
		d->GridX = nstr.section(',',0,0).toInt(&ok); if(ok) {
		d->GridY = nstr.section(',',1,1).toInt(&ok); if(ok) {
		if(nstr.section(',',2,2).toInt(&ok) == 0) d->GridOn = false;
		else d->GridOn = true; }} }
    else if(cstr == "DataSet") d->DataSet = nstr;
    else if(cstr == "DataDisplay") d->DataDisplay = nstr;
    else if(cstr == "OpenDisplay")
		if(nstr.toInt(&ok) == 0) d->SimOpenDpl = false;
		else d->SimOpenDpl = true;
    else if(cstr == "Script") d->Script = nstr;
    else if(cstr == "RunScript")
		if(nstr.toInt(&ok) == 0) d->SimRunScript = false;
		else d->SimRunScript = true;
    else if(cstr == "showFrame")
		d->setFrameType( nstr.at(0).toLatin1() - '0');
    else if(cstr == "FrameText0") d->setFrameText(0, nstr);
    else if(cstr == "FrameText1") d->setFrameText(1, nstr);
    else if(cstr == "FrameText2") d->setFrameText(2, nstr);
    else if(cstr == "FrameText3") d->setFrameText(3, nstr);
    else {
      QMessageBox::critical(0, QObject::tr("Error"),
	   QObject::tr("Format Error:\nUnknown property: ")+cstr);
      return false;
    }
    if(!ok) {
      QMessageBox::critical(0, QObject::tr("Error"),
	   QObject::tr("Format Error:\nNumber expected in property field!"));
      return false;
    }
  }

  QMessageBox::critical(0, QObject::tr("Error"),
               QObject::tr("Format Error:\n'Property' field is not closed!"));
  return false;
}
#endif

// ---------------------------------------------------
// Inserts a component without performing logic for wire optimization.
#if 0
void Schematic::simpleInsertComponent(Component *c)
{
#if 0
  // assert(&_doc->components() == &components());
  // assert(&_doc->nodes() == &nodes());
  int x, y;
  // connect every node of component
  for(auto pp : c->Ports){
    int x=pp->x+c->cx_();
    int y=pp->y+c->cy_();
    qDebug() << c->label() << "port" << x << y;

    // check if new node lies upon existing node
    for(auto pn_ : nodes()){
      pn = pn_;
      if(pn->cx_() == x) if(pn->cy_() == y) {
// 	if (!pn->DType.isEmpty()) {
// 	  pp->Type = pn->DType;
// 	}
// 	if (!pp->Type.isEmpty()) {
// 	  pn->DType = pp->Type;
// 	}
	break;
      }
    }

    if(!pn) {
      // create new node, if no existing one lies at this position
      pn = new Node(x, y);
      nodes().append(pn);
    }else{
    }

    pn->Connections.append(c);  // connect schematic node to component node
    if (!pp->Type.isEmpty()) {
//      pn->DType = pp->Type;
    }

    pp->Connection = pn;  // connect component node to schematic node
  }

  components().append(c);
#endif
}
#endif

// todo: proper string processing
static std::string find_type_in_string(QString& Line)
{
  Line = Line.trimmed();
  if(Line.at(0) != '<') {
    QMessageBox::critical(0, QObject::tr("Error"),
			QObject::tr("Format Error:\nWrong line start!"));
    incomplete();
    // throw exception_CS?
  //  return 0;
  }

  QString cstr = Line.section (' ',0,0); // component type
  cstr.remove (0,1);    // remove leading "<"

  return cstr.toLatin1().data();
}

// -------------------------------------------------------------
bool SchematicModel::loadComponents(QTextStream *stream)
{
  assert(false);
  unreachable();
#if 0
  void* List=nullptr; // incomplete
  QString Line, cstr;
  Component *c;
  while(!stream->atEnd()) {
    Line = stream->readLine();
    if(Line.at(0) == '<' && Line.at(1) == '/'){
      // ?!
      return true;
    }
    Line = Line.trimmed();
    if(Line.isEmpty()) continue;

    /// \todo enable user to load partial schematic, skip unknown components
    qDebug() << "loadline" << Line;
    Element* e = getComponentFromName(Line);

    if(Command* cc=command(e)){
      qDebug() << "got command";
      simpleInsertCommand(cc);
    }else if(Component* c=component(e)){
     // c->setSchematic(this);
	
      if(List) {  // "paste" ?
//	int z;
//	for(z=c->name().length()-1; z>=0; z--) // cut off number of component name
//	  if(!c->name().at(z).isDigit()) break;
//	c->obsolete_name_override_hack(c->name().left(z+1));
//	List->append(c);
      }else{
	simpleInsertComponent(c);
      }
    }else{
      incomplete();
    }
  }

  QMessageBox::critical(0, QObject::tr("Error"),
	   QObject::tr("Format Error:\n'Component' field is not closed!"));
  return false;
#endif
}

// -------------------------------------------------------------
// Inserts a wire without performing logic for optimizing.
#if 0 // -> model
void Schemati::simpleInsertWire(Wire *pw)
{
  Node *pn=nullptr;

  // find_node_at
  for(auto pn_ : nodes()){
    if(pn_->cx_() == pw->x1_()) {
      if(pn_->cy_() == pw->y1_()) {
	pn = pn_;
	break;
      }
    }
  }

  if(!pn) {   // create new node, if no existing one lies at this position
    pn = new Node(pw->x1_(), pw->y1_());
    nodes().append(pn);
  }

  if(pw->x1_() == pw->x2_()) if(pw->y1_() == pw->y2_()) {
    pn->Label = pw->Label;   // wire with length zero are just node labels
    if (pn->Label) {
      pn->Label->Type = isNodeLabel;
      pn->Label->pOwner = pn;
    }
    // yikes.
    // delete pw;           // delete wire because this is not a wire
    // return;
  }
  pn->Connections.append(pw);  // connect schematic node to component node
  pw->Port1 = pn;

  // find_node_at
  pn=nullptr;
  for(auto pn_ : nodes()){
    if(pn_->cx_() == pw->x2_()) {
      if(pn_->cy_() == pw->y2_()) {
	pn = pn_;
	break;
      }
    }
  }

  if(!pn) {   // create new node, if no existing one lies at this position
    pn = new Node(pw->x2_(), pw->y2_());
    nodes().append(pn);
  }
  pn->Connections.append(pw);  // connect schematic node to component node
  pw->Port2 = pn;

  wires().append(pw);
}
#endif

// -------------------------------------------------------------
// obsolete.
bool SchematicModel::loadWires(QTextStream *stream /*, EGPList *List */)
{
  incomplete();
  unreachable();
  QList<ElementGraphics*>* List=nullptr; //?
  Wire *w;
  QString Line;
  while(!stream->atEnd()) {
    Line = stream->readLine();
    if(Line.at(0) == '<') if(Line.at(1) == '/') return true;
    Line = Line.trimmed();
    if(Line.isEmpty()) continue;

    // (Node*)4 =  move all ports (later on)
    w = new Wire(0,0,0,0, (Node*)4,(Node*)4);
    if(!w->obsolete_load(Line)) {
      QMessageBox::critical(0, QObject::tr("Error"),
		QObject::tr("Format Error:\nWrong 'wire' line format!"));
      delete w;
      return false;
    }
#if 0
    incomplete();
    if(List) {
      if(w->x1_() == w->x2_()) if(w->y1_() == w->y2_()) if(w->Label) {
	w->Label->Type = isMovingLabel;
#ifdef USE_SCROLLVIEW
	List->append(w->Label);
#else
	List->append(new ElementGraphics(w->Label));
#endif
	delete w;
	continue;
      }
#ifdef USE_SCROLLVIEW
      List->append(w);
#else
      List->append(new ElementGraphics(w));
#endif
      if(w->Label){
#ifdef USE_SCROLLVIEW
      	List->append(w);
#else
      	List->append(new ElementGraphics(w->Label));
#endif
      }else{
      }
#endif
    {
      simpleInsertWire(w);
    }
  }

  QMessageBox::critical(0, QObject::tr("Error"),
		QObject::tr("Format Error:\n'Wire' field is not closed!"));
  return false;
}

// -------------------------------------------------------------

bool SchematicModel::loadPaintings(QTextStream *stream, PaintingList*)
{
  incomplete();
  return false;
}

// -------------------------------------------------------------
bool PaintingList::load(QTextStream *stream)
{
  auto List=this;
  Painting *p=0;
  QString Line, cstr;
  while(!stream->atEnd()) {
    Line = stream->readLine();
    if(Line.at(0) == '<') if(Line.at(1) == '/') return true;

    Line = Line.trimmed();
    if(Line.isEmpty()) continue;
    if( (Line.at(0) != '<') || (Line.at(Line.length()-1) != '>')) {
      QMessageBox::critical(0, QObject::tr("Error"),
	QObject::tr("Format Error:\nWrong 'painting' line delimiter!"));
      return false;
    }
    Line = Line.mid(1, Line.length()-2);  // cut off start and end character

    cstr = Line.section(' ',0,0);    // painting type
    qDebug() << cstr;
    if(Painting const* pp=painting_dispatcher[cstr.toStdString()]){
      p=prechecked_cast<Painting*>(pp->clone());
      assert(p);
    }else{
      QMessageBox::critical(0, QObject::tr("Error"),
		QObject::tr("Format Error:\nUnknown painting ") + cstr);
      return false;
    }

    if(!p->load(Line)) {
      QMessageBox::critical(0, QObject::tr("Error"),
	QObject::tr("Format Error:\nWrong 'painting' line format!"));
      delete p;
      return false;
    }
    // BUG. this is necessary for subcircuit parameters
    List->append(p);
  }

  QMessageBox::critical(0, QObject::tr("Error"),
	QObject::tr("Format Error:\n'Painting' field is not closed!"));
  return false;
}

/*!
 * \brief Schematic::loadDocument tries to load a schematic document.
 * \return true/false in case of success/failure
 */
bool Schematic::loadDocument()
{
  QFile file(docName());
  qDebug() << "opening" << docName();
  if(!file.open(QIODevice::ReadOnly)) { untested();
    /// \todo implement unified error/warning handling GUI and CLI
    if (QucsMain)
      QMessageBox::critical(0, QObject::tr("Error"),
                 QObject::tr("Cannot load document: ")+docName());
    else
      qCritical() << "Schematic::loadDocument:"
                  << QObject::tr("Cannot load document: ")+docName();
    return false;
  }else{
    DocModel.setFileInfo(docName());

    DocModel.loadDocument(file);
    // scene()->loadModel(DocModel); // ??
#ifndef USE_SCROLLVIEW
    QGraphicsScene& s=*scene();
//    DocModel.toScene(s);
#endif
    return true;
  }
}

// -------------------------------------------------------------
// Creates a Qucs file format (without document properties) in the returning
// string. This is used to save state for undo operation.
QString Schematic::createUndoString(char Op)
{
  Painting *pp;

  // Build element document.
  QString s = "  \n";
  s.replace(0,1,Op);
  for(auto pc : components()){
    QTextStream str(&s);
    incomplete();
    // SchematicModel::saveComponent(str, pc);
    s += "\n";
  }
  s += "</>\n";  // short end flag

  for(auto pw : wires()){
    s += pw->save()+"\n";
  }
  // save all labeled nodes as wires
  for(auto pn : nodes()) {
    if(pn->Label) s += pn->Label->save()+"\n";
  }
  s += "</>\n";

  for(auto pd : diagrams()){
    s += pd->save()+"\n";
  }
  s += "</>\n";

  for(pp = paintings().first(); pp != 0; pp = paintings().next())
    s += "<"+pp->save()+">\n";
  s += "</>\n";

  return s;
}

// -------------------------------------------------------------
// Same as "createUndoString(char Op)" but for symbol edit mode.
QString Schematic::createSymbolUndoString(char Op)
{
  Painting *pp;

  // Build element document.
  QString s = "  \n";
  s.replace(0,1,Op);
  s += "</>\n";  // short end flag for components
  s += "</>\n";  // short end flag for wires
  s += "</>\n";  // short end flag for diagrams

  for(pp = symbolPaintings().first(); pp != 0; pp = symbolPaintings().next())
    s += "<"+pp->save()+">\n";
  s += "</>\n";

  return s;
}

class ModelStream : public QTextStream {
public:
  explicit ModelStream(QString /* BUG const */ * filename, QIODevice::OpenModeFlag flag) :
    QTextStream(filename, flag){}

};

class ParseError : public std::exception{
};

#if 0
static void parser_temporary_kludge(SchematicModel& m, ModelStream& stream)
{
  if(!m.loadComponents(&stream)) throw ParseError();
  if(!m.loadWires(&stream))  throw ParseError();
  if(!m.loadDiagrams(&stream /* wtf?, &DocDiags */)) throw ParseError();
  if(!m.loadPaintings(&stream)) throw ParseError();

}
#endif


// -------------------------------------------------------------
// Is quite similiar to "loadDocument()" but with less error checking.
// Abused for "undo" function.
bool Schematic::rebuild(QString *s)
{
  incomplete();
  DocModel.clear();

  QString Line;
  qDebug() << "rebuild. opening" << *s;
  DocumentStream stream(s, QIODevice::ReadOnly);
  Line = stream.readLine();  // skip identity byte

  // read content *************************
  DocModel.parse(stream);;

  return true;
}

// -------------------------------------------------------------
// Same as "rebuild(QString *s)" but for symbol edit mode.
bool Schematic::rebuildSymbol(QString *s)
{
  symbolPaintings().clear();	// delete whole document

  QString Line;
  qDebug() << "rebuild. opening" << *s;
  QTextStream stream(s, QIODevice::ReadOnly);
  Line = stream.readLine();  // skip identity byte

  // read content *************************
  Line = stream.readLine();  // skip components
  Line = stream.readLine();  // skip wires
  Line = stream.readLine();  // skip diagrams
  if(!symbolPaintings().load(&stream)) return false;

  return true;
}


// ***************************************************************
// *****                                                     *****
// *****             Functions to create netlist             *****
// *****                                                     *****
// ***************************************************************

void createNodeSet(QStringList& Collect, int& countInit,
			      Conductor *pw, Node *p1)
{
  if(pw->Label)
    if(!pw->Label->initValue.isEmpty())
      Collect.append("NodeSet:NS" + QString::number(countInit++) + " " +
                     p1->name() + " U=\"" + pw->Label->initValue + "\"");
}

// ---------------------------------------------------
// find connected components (slow)
#if 0
void Schematic::throughAllNodes(unsigned& z) const
{ untested();
  z = 0; // number cc.
  bool isAnalog=true; //?!
  Node *pn=nullptr;
  int z=0;

  for(auto pn : nodes()){
    assert(!pn->hasNumber());
  }

  for(auto pn : nodes()){
    if(pn->hasNumber()){
    }else{
      pn->setNumber(z++);
      propagateNode(pn);
    }
  }

  qDebug() << "got" << nodes().count() << "nodes and" << z << "cc";
  nc = z;
} // throughAllNodes
#endif

// ----------------------------------------------------------
// Checks whether this file is a qucs file and whether it is an subcircuit.
// It returns the number of subcircuit ports.
int Schematic::testFile(const QString& DocName)
{
  QFile file(DocName);
  if(!file.open(QIODevice::ReadOnly)) {
    return -1;
  }

  QString Line;
  // .........................................
  // To strongly speed up the file read operation the whole file is
  // read into the memory in one piece.
  QTextStream ReadWhole(&file);
  QString FileString = ReadWhole.readAll();
  file.close();
  qDebug() << "ReadWhole opening" << FileString;
  QTextStream stream(&FileString, QIODevice::ReadOnly);


  // read header ........................
  do {
    if(stream.atEnd()) {
      file.close();
      return -2;
    }
    Line = stream.readLine();
    Line = Line.trimmed();
  } while(Line.isEmpty());

  if(Line.left(16) != "<Qucs Schematic ") {  // wrong file type ?
    file.close();
    return -3;
  }

  Line = Line.mid(16, Line.length()-17);
  VersionTriplet DocVersion = VersionTriplet(Line);
  if (DocVersion > QucsVersion) { // wrong version number ?
      if (!QucsSettings.IgnoreFutureVersion) {
          file.close();
          return -4;
      }
    //file.close();
    //return -4;
  }

  // read content ....................
  while(!stream.atEnd()) {
    Line = stream.readLine();
    if(Line == "<Components>") break;
  }

  int z=0;
  while(!stream.atEnd()) {
    Line = stream.readLine();
    if(Line == "</Components>") {
      file.close();
      return z;       // return number of ports
    }

    Line = Line.trimmed();
    QString s = Line.section(' ',0,0);    // component type
    if(s == "<Port") z++;
  }
  return -5;  // component field not closed
}

// ---------------------------------------------------
// Collects the signal names for digital simulations.
void SchematicModel::collectDigitalSignals(void)
{
  incomplete();
// Node *pn=nullptr;
//
//  for(pn = nodes().first(); pn != 0; pn = nodes().next()) {
//    DigMap::Iterator it = Signals.find(pn->name());
//    if(it == Signals.end()) { // avoid redeclaration of signal
//      Signals.insert(pn->name(), DigSignal(pn->name(), pn->DType));
//    } else if (!pn->DType.isEmpty()) {
//      it.value().Type = pn->DType;
//    }
//  }
}

<<<<<<< HEAD
// ---------------------------------------------------
// Propagates the given node to connected component ports.
void SchematicModel::propagateNode(Node *pn) const
{ untested();
  bool setName=false;
  std::deque<Node*> q;
  Node *p2;
  Element *pe;

  assert(pn->hasNumber());
  int z=pn->number();

  q.push_back(pn);
  while(!q.empty()){
    auto cur=q.front();
    q.pop_front();
    cur->setNumber(z);

    for(auto pe : cur->Connections){
      if(Wire* pw=wire(pe)){ untested();
	if(cur == pw->Port2) { untested();
	  if(!pw->Port1->hasNumber()){
	    q.push_back(pw->Port1);
	  }else{
	  }
	}else if(cur == pw->Port1) { untested();
	  if(!pw->Port2->hasNumber()){
	    q.push_back(pw->Port2);
	  }else{
	  }
	}else{
	  // wires have exactly 2 connections.
	  assert(false);
	}
      }
    }
  }
}

#include <iostream>

/*!
 * \brief Schematic::throughAllComps
 * Goes through all schematic components and allows special component
 * handling, e.g. like subcircuit netlisting.
 * \param stream is a pointer to the text stream used to collect the netlist
 * \param countInit is the reference to a counter for nodesets (initial conditions)
 * \param Collect is the reference to a list of collected nodesets
 * \param ErrText is pointer to the QPlainTextEdit used for error messages
 * \param NumPorts counter for the number of ports
 * \return true in case of success (false otherwise)
 */

// BUG: produces output
// to be called from qucsator language only..
#if 0 // moved to qucsator.cc
bool Schematic::throughAllComps(QTextStream *stream, int& countInit...);
#endif

// ---------------------------------------------------
// Follows the wire lines in order to determine the node names for
// each component. Output into "stream", NodeSets are collected in
// "Collect" and counted with "countInit".
//
// BUG prints stuff.
bool SchematicModel::giveNodeNames(DocumentStream& stream, int& countInit,
                   QStringList& Collect, QPlainTextEdit *ErrText, int NumPorts,
		   bool creatingLib,
		   NetLang const& nl)
{ untested();
  incomplete();
  return 0;
#if 0
  // delete the node names
  for(auto pn : nodes()) {
    pn->State = 0;
    if(pn->Label) {
      if(isAnalog){
	qDebug() << "attach label" << pn->Label->name();
        pn->setName(pn->Label->name());
        pn->setLabel(pn->Label->name());
        // pn->setNumber(pn->Label->number());
      } else{
	assert(0);
	incomplete();
        pn->setName("net" + pn->Label->name());
      }
    } else {
      pn->setName("");
    }
  }

  // set the wire names to the connected node
  for(auto pw : wires()){
    if(pw->Label != 0) {
      if(isAnalog)
        pw->Port1->setName(pw->Label->name());
      else  // avoid to use reserved VHDL words
        pw->Port1->setName("net" + pw->Label->name());
    }
  }

  // go through components
  // BUG: ejects declarations
  if(!throughAllComps(stream, countInit, Collect, ErrText, NumPorts, creatingLib, nl)){
    fprintf(stderr, "Error: Could not go throughAllComps\n");
    return false;
  }

  // number connected components
  unsigned z=0;
  throughAllNodes(z);
  countInit = z;

  if(!isAnalog){
    incomplete();
    // collect all node names for VHDL signal declaration
    // collectDigitalSignals();
  }

  return true;
#endif
}

// ---------------------------------------------------
bool SchematicModel::createLibNetlist(DocumentStream& stream, QPlainTextEdit
    *ErrText, int NumPorts, NetLang const& nl)
{
  qDebug() << "createLibNetlist";
  bool isAnalog=true;
  bool isVerilog=false;
  DigMap Signals; //?!
  int countInit = 0;
  QStringList Collect;
  Collect.clear();
  FileList.clear();
  Signals.clear();
  // Apply node names and collect subcircuits and file include
  bool creatingLib = true;
  if(!giveNodeNames(stream, countInit, Collect, ErrText, NumPorts, creatingLib, nl)) {
    creatingLib = false;
    return false;
  }else{
  }
  creatingLib = false;

  // Marking start of actual top-level subcircuit
  QString c;
  if(!isAnalog) {
    if (isVerilog)
      c = "///";
    else
      c = "---";
  }
  else c = "###";
  stream << "\n" << c << " TOP LEVEL MARK " << c << "\n";

  // Emit subcircuit components
  incomplete();
  createSubNetlistPlain(stream, ErrText, NumPorts, creatingLib);

  Signals.clear();  // was filled in "giveNodeNames()"
  return true;
} // createLibNetlist

//#define VHDL_SIGNAL_TYPE "bit"
//#define VHDL_LIBRARIES   ""
static const std::string VHDL_SIGNAL_TYPE("std_logic");
static const std::string VHDL_LIBRARIES("\nlibrary ieee;\nuse ieee.std_logic_1164.all;\n");

// ---------------------------------------------------
void SchematicModel::createSubNetlistPlain(DocumentStream& stream, QPlainTextEdit *ErrText,
int NumPorts, bool creatingLib)
{
  qDebug() << "createSubNetlistPlain" << creatingLib;
  DigMap Signals; //?!
  bool isAnalog=true;
  bool isVerilog=false;
  int i, z;
  QString s;
  QStringList SubcircuitPortNames;
  QStringList SubcircuitPortTypes;
  QStringList InPorts;
  QStringList OutPorts;
  QStringList InOutPorts;
  QStringList::iterator it_name;
  QStringList::iterator it_type;
  Component *pc;

  // probably creating a library currently
  QTextStream * tstream = &stream;
  QFile ofile;
  if(creatingLib) {
    incomplete();
    //QString f = misc::properAbsFileName(docName()) + ".lst";
    QString f = misc::properAbsFileName("INCOMPLETE.lst");
    ofile.setFileName(f);
    if(!ofile.open(IO_WriteOnly)) {
      incomplete();
      throw "something wrong with lib, incomplete";
      //ErrText->appendPlainText(tr("ERROR: Cannot create library file \"%s\".").arg(f));
    }
    tstream = new QTextStream(&ofile);
  }else{
    qDebug() << "createSubNetlistPlain, nolib";
  }

  // collect subcircuit ports and sort their node names into
  // "SubcircuitPortNames"
  PortTypes.clear();
  for(auto pc : components()){
    if(dynamic_cast<Command const*> (pc)){
      // ignore commands.
    }else if(pc->obsolete_model_hack() == "Port") {
      i = pc->Props.first()->Value.toInt();
      for(z=SubcircuitPortNames.size(); z<i; z++) { // add empty port names
        SubcircuitPortNames.append(" ");
        SubcircuitPortTypes.append(" ");
      }
      it_name = SubcircuitPortNames.begin();
      it_type = SubcircuitPortTypes.begin();
      for(int n=1;n<i;n++)
      {
        it_name++;
        it_type++;
      }
      (*it_name) = pc->Ports.first()->Connection->name();
      qDebug() << "found" << *it_name;
      DigMap::Iterator it = Signals.find(*it_name);
      if(it!=Signals.end())
        (*it_type) = it.value().Type;
      // propagate type to port symbol
      // pc->Ports.first()->Connection->DType = *it_type;

      if(!isAnalog) {
        if (isVerilog) {
          Signals.remove(*it_name); // remove node name
          switch(pc->Props.at(1)->Value.at(0).toLatin1()) {
            case 'a':
              InOutPorts.append(*it_name);
              break;
            case 'o':
              OutPorts.append(*it_name);
              break;
              default:
                InPorts.append(*it_name);
          }
        }
        else {
          // remove node name of output port
          Signals.remove(*it_name);
          switch(pc->Props.at(1)->Value.at(0).toLatin1()) {
            case 'a':
              (*it_name) += " : inout"; // attribute "analog" is "inout"
              break;
            case 'o': // output ports need workaround
              Signals.insert(*it_name, DigSignal(*it_name, *it_type));
              (*it_name) = "net_out" + (*it_name);
              // fall through
            default:
              (*it_name) += " : " + pc->Props.at(1)->Value;
          }
          (*it_name) += " " + ((*it_type).isEmpty() ?
          QString::fromStdString(VHDL_SIGNAL_TYPE) : (*it_type));
        }
      }
    }else{
      assert(pc->obsolete_model_hack() != "Port");
    }
  }
  qDebug() << "subckt port stuff";

  // remove empty subcircuit ports (missing port numbers)
  for(it_name = SubcircuitPortNames.begin(),
      it_type = SubcircuitPortTypes.begin();
      it_name != SubcircuitPortNames.end(); ) {
    if(*it_name == " ") {
      it_name = SubcircuitPortNames.erase(it_name);
      it_type = SubcircuitPortTypes.erase(it_type);
    } else {
      PortTypes.append(*it_type);
      it_name++;
      it_type++;
    }
  }

  QString f;
  if(doc()){
    //???
    QString f = misc::properFileName(doc()->docName());
  }else{
    f = devType();
  }
  QString Type = misc::properName(f);
  qDebug() << "propername?" << Type;

  Painting *pi;
  if(isAnalog) {
    qDebug() << "subckt definition";
    // ..... analog subcircuit ...................................
    (*tstream) << "\n.Def:" << Type << " " << SubcircuitPortNames.join(" ");
    for(pi = symbolPaintings().first(); pi != 0; pi = symbolPaintings().next())
      if(pi->Name == ".ID ") {
        ID_Text *pid = (ID_Text*)pi;
        QList<SubParameter *>::const_iterator it;
        for(it = pid->Parameter.constBegin(); it != pid->Parameter.constEnd(); it++) {
          s = (*it)->Name; // keep 'Name' unchanged
          (*tstream) << " " << s.replace("=", "=\"") << '"';
        }
        break;
      }
    (*tstream) << '\n';

    auto nlp=doclang_dispatcher["qucsator"];
    NetLang const* n=prechecked_cast<NetLang const*>(nlp);
    assert(n);
    NetLang const& nl=*n;

    // write all components with node names into netlist file
    for(auto pc : components() ){ untested();
      qDebug() << "comps" << pc->type();
      if(dynamic_cast<Port const*>(pc)){
	  incomplete();
      }else if(pc->type() == "GND") { untested();
      }else{
	try{
	  // fixme.
	  (*tstream) << pc->getNetlist();
	}catch(std::exception const&){
	  nlp->printItem(pc, *tstream);
	}
      }
    }

    (*tstream) << ".Def:End\n";

  } else {
    incomplete();
    if (isVerilog) {
      incomplete(); // use verilog
      // ..... digital subcircuit ...................................
      (*tstream) << "\nmodule Sub_" << Type << " ("
              << SubcircuitPortNames.join(", ") << ");\n";

      // subcircuit in/out connections
      if(!InPorts.isEmpty())
        (*tstream) << " input " << InPorts.join(", ") << ";\n";
      if(!OutPorts.isEmpty())
        (*tstream) << " output " << OutPorts.join(", ") << ";\n";
      if(!InOutPorts.isEmpty())
        (*tstream) << " inout " << InOutPorts.join(", ") << ";\n";

      // subcircuit connections
      if(!Signals.isEmpty()) {
        QList<DigSignal> values = Signals.values();
        QList<DigSignal>::const_iterator it;
        for (it = values.constBegin(); it != values.constEnd(); ++it) {
          (*tstream) << " wire " << (*it).Name << ";\n";
        }
      }
      (*tstream) << "\n";

      // subcircuit parameters
      for(pi = symbolPaintings().first(); pi != 0; pi = symbolPaintings().next())
        if(pi->Name == ".ID ") {
          QList<SubParameter *>::const_iterator it;
          ID_Text *pid = (ID_Text*)pi;
          for(it = pid->Parameter.constBegin(); it != pid->Parameter.constEnd(); it++) {
            s = (*it)->Name.section('=', 0,0);
            QString v = misc::Verilog_Param((*it)->Name.section('=', 1,1));
            (*tstream) << " parameter " << s << " = " << v << ";\n";
          }
          (*tstream) << "\n";
          break;
        }

      // write all equations into netlist file
      for(auto pc : components()){
        if(pc->obsolete_model_hack() == "Eqn") {
          (*tstream) << pc->get_Verilog_Code(NumPorts);
        }
      }

      if(Signals.find("gnd") != Signals.end())
      (*tstream) << " assign gnd = 0;\n"; // should appear only once

      // write all components into netlist file
      for(auto pc : components()){
        if(pc->obsolete_model_hack() != "Eqn") {
          s = pc->get_Verilog_Code(NumPorts);
          if(s.length()>0 && s.at(0) == '\xA7') {  //section symbol
            ErrText->appendPlainText(s.mid(1));
          }
          else (*tstream) << s;
        }
      }

      (*tstream) << "endmodule\n";
    } else {
      // ..... digital subcircuit ...................................
      (*tstream) << QString::fromStdString(VHDL_LIBRARIES);
      (*tstream) << "entity Sub_" << Type << " is\n"
                << " port ("
                << SubcircuitPortNames.join(";\n ") << ");\n";

      for(pi = symbolPaintings().first(); pi != 0; pi = symbolPaintings().next())
        if(pi->Name == ".ID ") {
          ID_Text *pid = (ID_Text*)pi;
          QList<SubParameter *>::const_iterator it;

          if (pid->Parameter.size()) {
            (*tstream) << " generic (";
            for(it = pid->Parameter.constBegin(); it != pid->Parameter.constEnd(); it++) {
              s = (*it)->Name;
              QString t = (*it)->Type.isEmpty() ? "real" : (*it)->Type;
              (*tstream) << s.replace("=", " : "+t+" := ") << ";\n ";
            }
            (*tstream) << ");\n";
          }
          break;
        }

      (*tstream) << "end entity;\n"
                  << "use work.all;\n"
                  << "architecture Arch_Sub_" << Type << " of Sub_" << Type
                  << " is\n";

      if(!Signals.isEmpty()) {
        QList<DigSignal> values = Signals.values();
        QList<DigSignal>::const_iterator it;
        for (it = values.constBegin(); it != values.constEnd(); ++it) {
          (*tstream) << " signal " << (*it).Name << " : "
          << ((*it).Type.isEmpty() ?
	      QString::fromStdString(VHDL_SIGNAL_TYPE) : (*it).Type) << ";\n";
        }
      }

      // write all equations into netlist file
      for(auto pc : components()){
        if(pc->obsolete_model_hack() == "Eqn") {
          ErrText->appendPlainText(
                      QObject::tr("WARNING: Equations in \"%1\" are 'time' typed.").
          arg(pc->name()));
          (*tstream) << pc->get_VHDL_Code(NumPorts);
        }
      }

      (*tstream) << "begin\n";

      if(Signals.find("gnd") != Signals.end())
      (*tstream) << " gnd <= '0';\n"; // should appear only once

      // write all components into netlist file
      for(auto pc : components()){
        if(pc->obsolete_model_hack() != "Eqn") {
            s = pc->get_VHDL_Code(NumPorts);
            if(s.length()>0 && s.at(0) == '\xA7') {  //section symbol
              ErrText->appendPlainText(s.mid(1));
          }
          else (*tstream) << s;
        }
      }

      (*tstream) << "end architecture;\n";
    }
  }

  // close file
  if(creatingLib) {
    ofile.close();
    delete tstream;
  }
}
// ---------------------------------------------------
// Write the netlist as subcircuit to the text stream 'stream'.
// BUG: not here.
bool SchematicModel::createSubNetlist(DocumentStream& stream, int& countInit,
                     QStringList& Collect, QPlainTextEdit *ErrText, int NumPorts,
		  bool creatingLib, NetLang const& nl)
{
  qDebug() << "createSubNetlist";
  DigMap Signals; //??
//  int Collect_count = Collect.count();   // position for this subcircuit

  // TODO: NodeSets have to be put into the subcircuit block.
  qDebug() << "giveNodeNames" << NumPorts << "\n";
  if(!giveNodeNames(stream, countInit, Collect, ErrText, NumPorts, creatingLib, nl)){
    fprintf(stderr, "Error giving NodeNames in createSubNetlist\n");
    return false;
  }else{
  }

/*  Example for TODO
      for(it = Collect.at(Collect_count); it != Collect.end(); )
      if((*it).left(4) == "use ") {  // output all subcircuit uses
        (*stream) << (*it);
        it = Collect.remove(it);
      }
      else it++;*/

  // Emit subcircuit components
  createSubNetlistPlain(stream, ErrText, NumPorts, creatingLib);

  // Signals.clear();  // was filled in "giveNodeNames()"
  return true;
}

// ---------------------------------------------------
// Determines the node names and writes subcircuits into netlist file.
// BUG. move to netlister.
int SchematicModel::prepareNetlist(DocumentStream& stream, QStringList& Collect,
                              QPlainTextEdit *ErrText, bool creatingLib, NetLang const& nl)
{
  incomplete();
  return 0;
#if 0
  //if(showBias > 0) showBias = -1;  // do not show DC bias anymore

  bool isVerilog = false;
  bool isAnalog = true;
  bool isTruthTable = false;
  int allTypes = 0, NumPorts = 0;

  // Detect simulation domain (analog/digital) by looking at component types.
  for(auto pc : components()){
    if(pc->isActive == COMP_IS_OPEN) continue;
    if(pc->obsolete_model_hack().at(0) == '.') {
      if(pc->obsolete_model_hack() == ".Digi") {
        if(allTypes & isDigitalComponent) {
          ErrText->appendPlainText(
             QObject::tr("ERROR: Only one digital simulation allowed."));
          return -10;
        }else if(pc->Props.getFirst()->Value != "TimeList"){
          isTruthTable = true;
	}
	if(pc->Props.getLast()->Value != "VHDL")
	        isVerilog = true;
        allTypes |= isDigitalComponent;
	      isAnalog = false;
      }else{
       	allTypes |= isAnalogComponent;
      }
      if((allTypes & isComponent) == isComponent) {
        ErrText->appendPlainText(
           QObject::tr("ERROR: Analog and digital simulations cannot be mixed."));
        return -10;
      }
    }else if(pc->obsolete_model_hack() == "DigiSource"){
      NumPorts++;
    }else{
    }
  }

  if((allTypes & isAnalogComponent) == 0) {
    if(allTypes == 0) {
      // If no simulation exists, assume analog simulation. There may
      // be a simulation within a SPICE file. Otherwise Qucsator will
      // output an error.
      isAnalog = true;
      allTypes |= isAnalogComponent;
      NumPorts = -1;
    }
    else {
      if(NumPorts < 1 && isTruthTable) {
        ErrText->appendPlainText(
           QObject::tr("ERROR: Digital simulation needs at least one digital source."));
        return -10;
      }
      if(!isTruthTable) NumPorts = 0;
    }
  }
  else {
    NumPorts = -1;
    isAnalog = true;
  }

  // first line is documentation
  if(allTypes & isAnalogComponent)
    stream << "#";
  else if (isVerilog)
    stream << "//";
  else{
    stream << "--";
  }

  stream << " Qucsator " << PACKAGE_VERSION << "\n"; //   " << docName() << "\n";

  // set timescale property for verilog schematics
  if (isVerilog) {
    stream << "\n`timescale 1ps/100fs\n";
  }

  int countInit = 0;  // counts the nodesets to give them unique names

  // BUG: giveNodeNames ejects subcircuit declarations (WTF?)
  if(!giveNodeNames(stream, countInit, Collect, ErrText, NumPorts, creatingLib, nl)){
    fprintf(stderr, "Error giving NodeNames\n");
    return -10;
  }

  if(allTypes & isAnalogComponent){
  }else if (!isVerilog) {
    stream << QString::fromStdString(VHDL_LIBRARIES);
    stream << "entity TestBench is\n"
	   << "end entity;\n"
	   << "use work.all;\n";
  }

  return NumPorts;
#endif
}

// ---------------------------------------------------
// Write the beginning of digital netlist to the text stream 'stream'.
// FIXME: really use lang. get rid of isVerilog
void Schematic::beginNetlistDigital(QTextStream& stream, NetLang const& /*lang*/)
{
  if (isVerilog) {
    stream << "module TestBench ();\n";
    QList<DigSignal> values = Signals.values();
    QList<DigSignal>::const_iterator it;
    for (it = values.constBegin(); it != values.constEnd(); ++it) {
      stream << "  wire " << (*it).Name << ";\n";
    }
    stream << "\n";
  } else {
    stream << "architecture Arch_TestBench of TestBench is\n";
    QList<DigSignal> values = Signals.values();
    QList<DigSignal>::const_iterator it;
    for (it = values.constBegin(); it != values.constEnd(); ++it) {
      stream << "  signal " << (*it).Name << " : "
	     << ((*it).Type.isEmpty() ?
		 QString::fromStdString(VHDL_SIGNAL_TYPE) : (*it).Type) << ";\n";
    }
    stream << "begin\n";
  }

  if(Signals.find("gnd") != Signals.end()) {
    if (isVerilog) {
      stream << "  assign gnd = 0;\n";
    } else {
      stream << "  gnd <= '0';\n";  // should appear only once
    }
  }
}

// ---------------------------------------------------
// Write the end of digital netlist to the text stream 'stream'.
// FIXME: use lang, not isVerilog
void Schematic::endNetlistDigital(QTextStream& stream, NetLang const& /*lang*/)
{
  if (isVerilog) {
  } else {
    stream << "end architecture;\n";
  }
}

// ---------------------------------------------------
// write all components with node names into the netlist file
// return some Time.
#if 0 // qucsatorNetlister::createNetlist
QString SchematicModel::createNetlist(DocumentStream& stream, int NumPorts, NetLang const& nl)
{
  return "incomplete";
  bool isAnalog=true;
  bool isVerilog=false;
  DigMap Signals;
  if(!isAnalog) {
    incomplete();
    // beginNetlistDigital(stream);
  }

  Signals.clear();  // was filled in "giveNodeNames()"
  FileList.clear();

  QString s, Time;
  for(auto pc : components()){
    if(isAnalog) {
      if(pc->type()=="GND"){ // qucsator hack
      }else{ untested();
	nl.printItem(pc, stream);
      }
    } else { // FIXME: use different lang to print things differently
      if(pc->obsolete_model_hack() == ".Digi" && pc->isActive) {  // simulation component ?
        if(NumPorts > 0) { // truth table simulation ?
	  if (isVerilog)
	    Time = QString::number((1 << NumPorts));
	  else
	    Time = QString::number((1 << NumPorts) - 1) + " ns";
        } else {
          Time = pc->Props.at(1)->Value;
	  if (isVerilog) {
	    if(!misc::Verilog_Time(Time, pc->name())) return Time;
	  } else {
	    if(!misc::VHDL_Time(Time, pc->name())) return Time;  // wrong time format
	  }
        }
      }
      if (isVerilog) {
	s = pc->get_Verilog_Code(NumPorts);
      } else {
	s = pc->get_VHDL_Code(NumPorts);
      }
      if (s.length()>0 && s.at(0) == '\xA7'){
          return s; // return error
      }
      stream << s;
    }
  }

  if(!isAnalog) {
    incomplete();
    //endNetlistDigital(stream);
  }else{
  }

  return Time;
}
#endif

// vim:ts=8:sw=2:noet
