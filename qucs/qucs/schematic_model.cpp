/***************************************************************************
                           schematic_model.cpp
                             ---------------
    begin                : 2018
    copyright            : Felix
    email                : felix@salfelder.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "schematic.h"
#include "schematic_lang.h"
#include "globals.h"

SchematicModel::SchematicModel(Schematic* s) : _doc(nullptr)
{
	// presumably Q3PTRlist without this is just a QList<*> (check)
	_symbol=new SchematicSymbol();
}

void SchematicModel::clear()
{
	// memory leak?!
	components().clear();
	nodes().clear();
	diagrams().clear();
	wires().clear();
	paintings().clear();
	//SymbolPaints.clear(); ??
}

QString const& SchematicModel::devType() const
{
	return DevType;
}

void SchematicModel::setDevType(QString const& s)
{
	DevType = s;
}

namespace{
class ins : public ModelInserter{
public:
	ins(SchematicModel* m) : _m(m) {
		assert(m);
	}
private: // ModelInserter
	void pushBack(Element* e){
		_m->pushBack(e);
	}
	void setParameter(std::string const&, std::string const&){
		incomplete();
	}
private:
	SchematicModel* _m;
};
}

void SchematicModel::parse(DocumentStream& s, SchematicLanguage const* L)
{ untested();
	if(!L){ untested();
		auto D=doclang_dispatcher["leg_sch"];
		L = dynamic_cast<SchematicLanguage const*>(D);
	}else{ untested();
	}
	assert(L);
	ins i(this);
	while(!s.atEnd()){ untested();
		qDebug() << "entering parse";
		L->parse(s, i);
		assert(s.atEnd()); // happens with legacy lang
	}
}

/// ACCESS FUNCTIONS.
// these are required to move model methods over to SchematicModel
// note that _doc->...() functions still involve pointer hacks
ComponentList& SchematicModel::components()
{
	return Components;
}

void SchematicModel::pushBack(Element* what)
{
	if(auto c=component(what)){ untested();
      simpleInsertComponent(c);
	}else if(auto d=diagram(what)){
		diagrams().append(d);
	}else if(auto w=wire(what)){
		simpleInsertWire(w);
	}else if(auto s=dynamic_cast<SchematicSymbol*>(what)){ untested();
		qDebug() << "Model replacing symbol";
		delete _symbol;
		_symbol = s;
	}else{
		incomplete();
	}

#ifndef USE_SCROLLVIEW
  if(doc()){
	  doc()->addToScene(what);
  }else{
  }
#endif
} // pushBack

// called from schematic::erase only
void SchematicModel::erase(Element* what)
{
	if(auto c=component(what)){
		components().removeRef(c);
	}else if(auto d=diagram(what)){
		diagrams().removeRef(d);
	}else if(auto w=wire(what)){
		wires().removeRef(w);
	}else{
		unreachable();
	}
	delete(what);
}

// should be a QucsDoc*, probably
Schematic* SchematicModel::doc()
{
	return _doc;
}

QFileInfo const& SchematicModel::getFileInfo ()const
{
	return FileInfo;
}

WireList& SchematicModel::wires()
{
	return Wires;
}

NodeList& SchematicModel::nodes()
{
	return Nodes;
}

PaintingList& SchematicModel::paintings()
{
	return Paintings;
}

PaintingList& SchematicModel::symbolPaintings()
{
	assert(_symbol);
	// temporary. move stuff here....
	return _symbol->symbolPaintings();
}

DiagramList& SchematicModel::diagrams()
{
	return Diagrams;
}

// same, but const.
ComponentList const& SchematicModel::components() const
{
	return Components;
}

WireList const& SchematicModel::wires() const
{
	return Wires;
}

NodeList const& SchematicModel::nodes() const
{
	return Nodes;
}

PaintingList const& SchematicModel::paintings() const
{
	return Paintings;
}

DiagramList const& SchematicModel::diagrams() const
{
	return Diagrams;
}

PaintingList const& SchematicModel::symbolPaints() const{
	return SymbolPaints;
}

static void createNodeSet(QStringList& Collect, int& countInit,
		Conductor *pw, Node *p1)
{
	if(pw->Label)
		if(!pw->Label->initValue.isEmpty())
			Collect.append("NodeSet:NS" + QString::number(countInit++) + " " +
					p1->name() + " U=\"" + pw->Label->initValue + "\"");
}

#if 0
bool SchematicModel::throughAllComps(DocumentStream& stream, int& countInit,
                   QStringList& Collect, QPlainTextEdit *ErrText, int NumPorts,
		   bool creatingLib, NetLang const& nl)
{
	bool r;
	QString s;
	bool isAnalog=true;
	bool isVerilog=false;

	// give the ground nodes the name "gnd", and insert subcircuits etc.
	for(auto pc : components()) {

		if(pc->isActive != COMP_IS_ACTIVE) continue;

		// check analog/digital typed components
		if(isAnalog) {
			if((pc->Type & isAnalogComponent) == 0) {
				//ErrText->appendPlainText(QObject::tr("ERROR: Component \"%1\" has no analog model.").arg(pc->name() ));
				return false;
			}
		} else {
			if((pc->Type & isDigitalComponent) == 0) {
				//ErrText->appendPlainText(QObject::tr("ERROR: Component \"%1\" has no digital model.").arg(pc->name() ));
				return false;
			}
		}

		auto dl=doclang_dispatcher["qucsator"];
		assert(dl);
		auto nl=dynamic_cast<NetLang const*>(dl);
		assert(dl);

		pc->tAC(stream, this, Collect, countInit, NumPorts, *nl); //?!!
		// handle ground symbol
		if(pc->obsolete_model_hack() == "GND") { // BUG.
			qDebug() << "GND hack" << pc->Ports.first()->Connection->name();
			pc->Ports.first()->Connection->setName("gnd");
			continue;
		}else if(pc->obsolete_model_hack() == "Sub") { untested();
			// moved to tAC
			continue; // BUG
		}else{
		}

#if 0 // does not work
		if(LibComp* lib = dynamic_cast</*const*/LibComp*>(pc)) {
			if(creatingLib) {
				ErrText->appendPlainText(
						QObject::tr("WARNING: Skipping library component \"%1\".").
						arg(pc->name()));
				continue;
			}
			QString scfile = pc->getSubcircuitFile();
			s = scfile + "/" + pc->Props.at(1)->Value;
			SubMap::Iterator it = FileList.find(s);
			if(it != FileList.end())
				continue;   // insert each library subcircuit just one time
			FileList.insert(s, SubFile("LIB", s));


			//FIXME: use different netlister for different purposes
			unsigned whatisit = isAnalog?1:(isVerilog?4:2);
			r = lib->createSubNetlist(stream, Collect, whatisit);
			if(!r) {
				ErrText->appendPlainText(
						QObject::tr("ERROR: \"%1\": Cannot load library component \"%2\" from \"%3\"").
						arg(pc->name(), pc->Props.at(1)->Value, scfile));
				return false;
			}
			continue; // BUG
		}
#endif

		// handle SPICE subcircuit components
		if(pc->obsolete_model_hack() == "SPICE") { // BUG
			incomplete();
#if 0
			s = pc->Props.first()->Value;
			// tell the spice component it belongs to this schematic
			pc->setSchematicModel (this);
			if(s.isEmpty()) {
				// ErrText->appendPlainText(QObject::tr("ERROR: No file name in SPICE component \"%1\".").
				//                 arg(pc->name()));
				return false;
			}
			QString f = pc->getSubcircuitFile();
			SubMap::Iterator it = FileList.find(f);
			if(it != FileList.end())
				continue;   // insert each spice component just one time
			FileList.insert(f, SubFile("CIR", f));

			SpiceFile *sf = (SpiceFile*)pc; // BUG
			incomplete(); // move to spicefile->tAC, then clean up
			// r = sf->createSubNetlist(stream);
			qDebug() << sf->getErrorText();
			if(!r){
				return false;
			}
			continue; // BUG
#endif
		} else if(pc->obsolete_model_hack() == "VHDL" || pc->obsolete_model_hack() == "Verilog") {
			incomplete();
#if 0
			if(isVerilog && pc->obsolete_model_hack() == "VHDL")
				continue;
			if(!isVerilog && pc->obsolete_model_hack() == "Verilog")
				continue;
			s = pc->Props.getFirst()->Value;
			if(s.isEmpty()) {
//				ErrText->appendPlainText(QObject::tr("ERROR: No file name in %1 component \"%2\".").
//						arg(pc->obsolete_model_hack()).
//						arg(pc->name()));
				return false;
			}
			QString f = pc->getSubcircuitFile();
			SubMap::Iterator it = FileList.find(f);
			if(it != FileList.end())
				continue;   // insert each vhdl/verilog component just one time
			s = ((pc->obsolete_model_hack() == "VHDL") ? "VHD" : "VER");
			FileList.insert(f, SubFile(s, f));

			if(pc->obsolete_model_hack() == "VHDL") {
				incomplete();
				//	VHDL_File *vf = (VHDL_File*)pc;
				//	r = vf->createSubNetlist(stream);
				//	ErrText->appendPlainText(vf->getErrorText());
				//	if(!r) {
				//	  return false;
				//	}
			}else if(pc->obsolete_model_hack() == "Verilog") {
				incomplete();
				//	Verilog_File *vf = (Verilog_File*)pc;
				//	r = vf->createSubNetlist(stream);
				//	ErrText->appendPlainText(vf->getErrorText());
				//	if(!r) {
				//	  return false;
				//	}
			}else{
			}
			continue; // BUG
#endif
		}
	}
	return true;
}
#endif

// not here.
void SchematicModel::throughAllNodes(bool User, QStringList& Collect,
		int& countInit)
{
	bool isAnalog=true; //?!
	Node *pn=nullptr;
	int z=0;

	for(pn = nodes().first(); pn != 0; pn = nodes().next()) {
		if(pn->name().isEmpty() == User) {
			continue;  // already named ?
		}
		if(!User) {
			if(isAnalog)
				pn->setName("_net" + QString::number(z++));
			else
				pn->setName("net_net" + QString::number(z++));
		}
		else if(pn->State) {
			continue;  // already worked on
		}

		if(isAnalog){
			createNodeSet(Collect, countInit, pn, pn);
		}else{
		}

		pn->State = 1;
		propagateNode(Collect, countInit, pn);
	}
}
#endif

#if 0
void SchematicModel::propagateNode(QStringList& Collect,
		int& countInit, Node *pn)
{
	bool isAnalog=true;
	bool setName=false;
	Q3PtrList<Node> Cons;
	Node *p2;
	Wire *pw;
	Element *pe;

	Cons.append(pn);
	for(p2 = Cons.first(); p2 != 0; p2 = Cons.next()){
		for(pe = p2->Connections.first(); pe != 0; pe = p2->Connections.next())
			if(wire(pe)){
				pw = (Wire*)pe;
				if(p2 != pw->Port1) {
					if(pw->Port1->name().isEmpty()) {
						pw->Port1->setName(pn->name());
						pw->Port1->State = 1;
						Cons.append(pw->Port1);
						setName = true;
					}
				}else{
					if(pw->Port2->name().isEmpty()) {
						assert(pn);
						pw->Port2->setName(pn->name());
						pw->Port2->State = 1;
						Cons.append(pw->Port2);
						setName = true;
					}
				}
				if(setName) {
					Cons.findRef(p2);   // back to current Connection
					if (isAnalog) createNodeSet(Collect, countInit, pw, pn);
					setName = false;
				}
			}
	}
	Cons.clear();
}
#endif

// really?
bool SchematicModel::giveNodeNames(DocumentStream& stream, int& countInit,
		QStringList& Collect, QPlainTextEdit *ErrText, int NumPorts,
		bool creatingLib, NetLang const& nl)
{
	incomplete();
	return false;
#if 0
	bool isAnalog=true;
	// delete the node names
	for(auto pn : nodes()) {
		pn->State = 0;
		if(pn->Label) {
			if(isAnalog)
				pn->setName(pn->Label->name());
			else
				pn->setName("net" + pn->Label->name());
		}
		else pn->setName("");
	}

	// set the wire names to the connected node
	for(auto pw : wires()){
		if(pw->Label != 0) {
			if(isAnalog)
				pw->Port1->setName(pw->Label->name());
			else  // avoid to use reserved VHDL words
				pw->Port1->setName("net" + pw->Label->name());
		}
	}

	// go through components
	// BUG: ejects declarations
	if(!throughAllComps(stream, countInit, Collect, ErrText, NumPorts, creatingLib, nl)){
		fprintf(stderr, "Error: Could not go throughAllComps\n");
		return false;
	}

	// work on named nodes first in order to preserve the user given names
	throughAllNodes(true, Collect, countInit);

	// give names to the remaining (unnamed) nodes
	throughAllNodes(false, Collect, countInit);

	if(!isAnalog) // collect all node names for VHDL signal declaration
		collectDigitalSignals();

	return true;
#endif
}

bool SchematicModel::loadDocument(QFile& /*BUG*/ file)
{ untested();
  QString Line;
  DocumentStream stream(&file);

  // read header **************************
  do {
    if(stream.atEnd()) {
      file.close(); // BUG
      return true;
    }

    Line = stream.readLine();
  } while(Line.isEmpty());

  if(Line.left(16) != "<Qucs Schematic ") {  // wrong file type ?
    file.close();
    throw "incomplete_exception";
  }

  Line = Line.mid(16, Line.length()-17);

  parse(stream);
  file.close();
  return true;
}

// called from PushBack...
void SchematicModel::simpleInsertComponent(Component *c)
{
	Node *pn;
	// connect every node of component
	for(auto pp : c->Ports){
		int x=pp->x+c->cx_();
		int y=pp->y+c->cy_();
		qDebug() << c->label() << "port" << x << y;

		// check if new node lies upon existing node
		for(pn = nodes().first(); pn != 0; pn = nodes().next()){
			if(pn->cx_() == x) if(pn->cy_() == y) {
				// 	if (!pn->DType.isEmpty()) {
				// 	  pp->Type = pn->DType;
				// 	}
				// 	if (!pp->Type.isEmpty()) {
				// 	  pn->DType = pp->Type;
				// 	}
				break;
			}
		}

		if(pn == 0) { // create new node, if no existing one lies at this position
			pn = new Node(x, y);
			nodes().append(pn);
		}
		pn->Connections.append(c);  // connect schematic node to component node
		if (!pp->Type.isEmpty()) {
			//      pn->DType = pp->Type;
		}

		pp->Connection = pn;  // connect component node to schematic node
	}

	components().append(c);
}

void SchematicModel::simpleInsertWire(Wire *pw)
{
  Node *pn=nullptr;

  // find_node_at
  for(auto pn_ : nodes()){
    if(pn_->cx_() == pw->x1_()) {
      if(pn_->cy_() == pw->y1_()) {
	pn = pn_;
	break;
      }
    }
  }

  if(!pn) {   // create new node, if no existing one lies at this position
    pn = new Node(pw->x1_(), pw->y1_());
    nodes().append(pn);
  }

  if(pw->x1_() == pw->x2_()) if(pw->y1_() == pw->y2_()) {
    pn->Label = pw->Label;   // wire with length zero are just node labels
    if (pn->Label) {
      pn->Label->Type = isNodeLabel;
      pn->Label->pOwner = pn;
    }
#if 0 // gaaah
    delete pw;           // delete wire because this is not a wire
    return;
#endif
  }
  pn->Connections.append(pw);  // connect schematic node to component node
  pw->Port1 = pn;

  // find_node_at
  pn=nullptr;
  for(auto pn_ : nodes()){
    if(pn_->cx_() == pw->x2_()) {
      if(pn_->cy_() == pw->y2_()) {
	pn = pn_;
	break;
      }
    }
  }

  if(!pn) {   // create new node, if no existing one lies at this position
    pn = new Node(pw->x2_(), pw->y2_());
    nodes().append(pn);
  }
  pn->Connections.append(pw);  // connect schematic node to component node
  pw->Port2 = pn;

  wires().append(pw);
}

ModelAccess::ModelAccess(){}
ModelInserter::ModelInserter(){}
//
// needed?
void SchematicModel::merge(SchematicModel& src)
{
  for(auto i : src.components()){ itested();
	  components().append(i);
  }
  src.components().clear();
}
